;;;; package.lisp

(mgl-pax:define-package #:yah
    (:use #:cl #:mgl-pax)
  (:export #:finger #:heap #:make-heap #:insert! #:extract! #:insert-extract!
           #:extract-insert! #:peek #:change-key! #:empty-heap? #:delete!))

(in-package #:yah)

(defsection @yah (:title "YAH - Yet Another Heap")
  (yah asdf/system:system)
  (@reference section))

(defsection @reference (:title "Reference")
  (finger class)
  (heap class)
  (make-heap function)
  (insert! function)
  (extract! function)
  (insert-extract! function)
  (extract-insert! function)
  (peek function)
  (change-key! function)
  (delete! function)
  (empty-heap? function))

(register-doc-in-pax-world :yah (list @yah) ())

(defun write-readme ()
  (update-asdf-system-readmes @yah "yah"))
