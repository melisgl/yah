;;;; yah.lisp

(in-package #:yah)

(defclass finger ()
  ((%key :initarg :key
         :accessor key<-)
   (%value :initarg :value
           :accessor value<-)
   (%index :initarg :index
           :accessor index<-)))

(defclass heap ()
  ((%content :type (array finger 1)
             :initform (make-array 0
                                   :element-type 'finger
                                   :adjustable t
                                   :fill-pointer t)
             :reader content<-)
   (%predicate :type function
               :initarg :predicate
               :reader predicate<-)))

(defun make-heap (predicate)
  "Makes a heap sorting by predicate `PREDICATE`. This means that if there are
  two values with keys A and B on the heap, if `(funcall predicate a b)` is
  true, A will be popped off the heap before B."
  (make-instance 'heap
                 :predicate predicate))

(defun heap-up! (heap i)
  (let ((content (content<- heap))
        (parent (floor (1- i)
                       2)))
    (when (and (>= parent 0)
               (funcall (predicate<- heap)
                        (key<- (elt content i))
                        (key<- (elt content parent))))
      (rotatef (elt content i)
               (elt content parent))
      (setf (index<- (elt content i))
            i
            (index<- (elt content parent))
            parent)
      (heap-up! heap parent))))

(defun heap-down! (heap i)
  (let* ((left (1+ (* i 2)))
         (right (1+ left))
         (content (content<- heap))
         (predicate (predicate<- heap))
         (length (fill-pointer content)))
    (when (< left length)
      (let ((key (key<- (elt content i)))
            (lkey (key<- (elt content left))))
        (unless (= (index<- (elt content i))
                   i))
        (if (< right length)
            (let ((rkey (key<- (elt content right))))
              (when (or (funcall predicate lkey key)
                        (funcall predicate rkey key))
                (if (funcall predicate lkey rkey)
                    (progn
                      (rotatef (elt content i)
                               (elt content left))
                      (setf (index<- (elt content i))
                            i
                            (index<- (elt content left))
                            left)
                      (heap-down! heap left))
                    (progn
                      (rotatef (elt content i)
                               (elt content right))
                      (setf (index<- (elt content i))
                            i
                            (index<- (elt content right))
                            right)
                      (heap-down! heap right)))))
            (when (funcall predicate lkey key)
              (rotatef (elt content i)
                       (elt content left))
              (setf (index<- (elt content i))
                    i
                    (index<- (elt content left))
                    left)))))))

(defun insert! (heap key value)
  "Add `VALUE` with at `KEY` into the `HEAP`. Returns a finger to the resulting
entry in the heap."
  (let* ((content (content<- heap))
         (length (fill-pointer content))
         (finger (make-instance 'finger
                                :key key
                                :value value
                                :index length)))
    (vector-push-extend finger content)
    (heap-up! heap length)
    finger))

(defun extract! (heap)
  "Pop the top element from the `HEAP`, according to the heap's
predicate. Returns two values: The value and the key of the popped element.

Fails if `HEAP` is empty."
  (let* ((content (content<- heap))
         (result (elt content 0)))
    (rotatef (elt content 0)
             (elt content (1- (fill-pointer content))))
    (setf (index<- (elt content 0))
          0)
    (vector-pop content)
    (heap-down! heap 0)
    (values (value<- result)
            (key<- result))))

(defun insert-extract! (heap key value)
  "Insert `VALUE` at `KEY` into `HEAP`, then immediately pop the top element."
  (let* ((content (content<- heap))
         (old (elt content 0))
         (old-key (key<- old)))
    (if (funcall (predicate<- heap)
                 key old-key)
        (values value key)
        (progn
          (setf (elt content 0)
                (make-instance 'finger
                               :key key
                               :value value
                               :index 0))
          (heap-down! heap 0)
          (values (value<- old)
                  old-key)))))

(defun extract-insert! (heap key value)
  "First pop the top element of `HEAP`, then insert `VALUE` at `KEY`. Returns
  two values: The value and the key of the popped element.

Fails if `HEAP` is empty."
  (let* ((content (content<- heap))
         (old (elt content 0)))
    (setf (elt content 0)
          (make-instance 'finger
                         :key key
                         :value value
                         :index 0))
    (heap-down! heap 0)
    (values (value<- old)
            (key<- old))))

(defun peek (heap)
  "Returns two values: The value and the key of the top element of the `HEAP`."
  (let ((result (elt (content<- heap)
                     0)))
    (values (value<- result)
            (key<- result))))

(defun change-key! (heap finger key)
  "Changes the key of `FINGER` in `HEAP` to `KEY`."
  (let ((old-key (key<- finger)))
    (setf (key<- finger)
          key)
    (if (funcall (predicate<- heap)
                 key old-key)
        (heap-down! heap (index<- finger))
        (heap-up! heap (index<- finger)))))

(defun delete! (heap finger)
  "Remove `FINGER` from `HEAP`."
  (let* ((index (index<- finger))
         (content (content<- heap))
         (end (1- (fill-pointer content)))
         (end-finger (elt content end)))
    (rotatef (elt content index)
             (elt content end))
    (setf (index<- end-finger)
          (index<- finger))
    (vector-pop content)
    (heap-up! heap (index<- end-finger))
    (heap-down! heap (print (index<- end-finger)))))

(defun empty-heap? (heap)
  "Returns `T` if `HEAP` is empty, and `NIL` otherwise."
  (zerop (fill-pointer (content<- heap))))
